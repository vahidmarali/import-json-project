## About Project

This project is based on laravel 8. The entire environment services that the project needs are present in the docker-compose.yml file and could be served by docker.

This project uses Laravel Sail to compose the environment.

Here is the instruction of installing and using this project:

###1 - Install packages and initialize docker containers
Clone the repository and run ```composer update``` with the installed php on your system.   
Go into the project's directory and then run ```./vendor/bin/sail up``` and wait for pulling and making containers and run them.

Run ```./vendor/bin/sail composer update``` to update vendor packages again with the installed php with docker to make sure that all the dependencies has been installed with correct versions.

###2 - Prepare System
Run ```./vendor/bin/sail artisan migrate``` and ```./vendor/bin/sail artisan key:generate``` during sail is up.

###3 - Configuration
All limits and conditions could be managed by modify ```config/customers.php```. there are five following settings in ```customers.php``` config file:
- ```min_age``` To limit minimum age of people to import. 
- ```max_age``` To limit maximum age of people to import.
- ```import_unknown_ages``` To set customers without information about their age should be imported or not.
- ```limit_cart_regex_pattern``` To put a Regular Expression pattern as required pattern for credit cards. For example setting it to ```/(.)\\1{2}/``` will import only customers with card number which contains three consecutive same digits.  
- ```import_string_length``` Because of the purpose of handling very big files, this project read json file using recursive stream reading file. this option limits the rate of the string that will be processed in every attempt. It depends on the power of the host machine.  

###4 - Json file
Put your json file wherever in ```storage``` directory.

###5 - Run the command to import
You can start importing file by running this command:
```./venodr/bin/sail artisan customer:import "[path to file]"```. File path is relative, and starts from ```storage``` directory. So if your file is located in ```./storage/app/challenge.json``` the command would be: <br>
```./venodr/bin/sail artisan customer:import "app/challenge.json"```

###6 - Run queue worker
This project uses ```Laravel Queue``` top handling import. So with development purpose you should run the command ```./vendor/bin/sail artisan queue:listen``` to make the project able to handle jobs.
For deploying and production purpose the command ```./vendor/bin/sail artisan queue:work``` should be run always, probably by using a service monitor system. If a power loose happen, the process will be continued from wherever it was in the last attempt.

##Extra Explanation
The project uses ```PDO``` and transactions to import records in the database And all queue jobs are unique. It's impossible to importing duplicate content because of this configuration.  

Reading json file is based on **[```PHP steream_get_contents```](https://www.php.net/manual/en/function.stream-get-contents.php)**. So it doesn't matter how big the file is. 1GB with a million record or even ten times bigger. But the time that it takes to import the entire file would be as long as the size of the file. 
