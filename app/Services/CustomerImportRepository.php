<?php


namespace App\Services;


interface CustomerImportRepository
{
    /**
     * @param array $customers
     */
    public function import(array $customers);
}
