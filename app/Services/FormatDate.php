<?php

namespace App\Services;

use Carbon\Carbon;

class FormatDate
{
    /**
     * @param string|null $date
     * @return string|null
     */
    public static function format(string|null $date): ?string
    {
        if ($date) {
            if (strlen($date) > 10)
                $carbon = new Carbon($date);
            else
                $carbon = Carbon::createFromFormat('d/m/Y', $date);
            $date = $carbon->format('Y-m-d');
        } else {
            $date = null;
        }

        return $date;
    }
}
