<?php


namespace App\Services;


use Illuminate\Support\Facades\Session;
use JetBrains\PhpStorm\NoReturn;
use JsonCollectionParser\ChunkListener;
use JsonMachine\JsonMachine;
use JsonMachine\Exception\UnexpectedEndSyntaxErrorException;
use JsonMachine\Exception\SyntaxError;

class JsonReaderStream implements JsonReaderInterface
{
    /**
     * @var string
     */
    protected string $path;

    /**
     * @var array
     */
    protected array $data = [];

    /**
     * @var int
     */
    protected int $next_offset;

    public function from(string $path): JsonReaderInterface
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @param array $item
     */
    public function setItem(array $item){
        $this->data[] = $item;
    }

    public function read(int $length = 0, int $offset = 0): array
    {
        $next_offset = 0;

        $file = fopen($this->path, 'r');
        $data = stream_get_contents($file, $length, $offset);

        if($offset)
            $data[0] = '[';

        $json = JsonMachine::fromString($data);


        //Put it to try to avoid UnexpectedEndSyntaxErrorException
        try {
            foreach ($json as $item){
                $next_offset = $offset+$json->getPosition();
                $this->data[] = $item;
            }
        }catch(\Exception $e){
            //Do nothing!
        }


        if(strlen($data))
            $this->next_offset = $next_offset;
        else
            $this->next_offset = 0;

        return $this->data;
    }

    public function nextOffset(): int
    {
        return $this->next_offset;
    }
}
