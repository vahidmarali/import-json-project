<?php

namespace App\Services;

use JsonMachine\JsonMachine;

class JsonReaderJsonMachine implements JsonReaderInterface
{
    /**
     * @var string
     */
    protected string $path;

    /**
     * @param string $path
     * @return JsonReaderJsonMachine
     */
    public function from(string $path): JsonReaderJsonMachine
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @param int $length
     * @return array
     */
    public function read(int $length = 0) : array
    {
        $json = JsonMachine::fromFile($this->path);

        $rows = [];

        $count = 0;
        foreach ($json as $json_part){

            $count++;

            if($count>=$offset)
                $rows[] = $json_part;

            if($length && $count===$offset+$length)
                break;
        }

        return $rows;
    }
}
