<?php

namespace App\Services;

use App\Models\CreditCard;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;
use PDO;

class CustomerImportPDO implements CustomerImportRepository
{
    /**
     * @var PDO
     */
    protected PDO $pdo;

    public function __construct()
    {
        $this->pdo = DB::connection()->getPdo();
    }

    /**
     * @param array $customer
     * @return int
     */
    protected function importCustomer(array $customer): int
    {
        //Format input date from json to a correct mysql date format Y-m-d
        $customer["date_of_birth"] = FormatDate::format($customer['date_of_birth']);

        //Because of PDO boolean data should be converted to integer zero or one
        $customer["checked"] = intval($customer["checked"]);

        $columns = (new Customer)->getFillable();

        $insert_columns = '`' . implode('`,`', $columns) . '`';
        $insert_params = ':' . implode(',:', $columns);

        $statement = $this->pdo->prepare("INSERT INTO `customers` ({$insert_columns}) VALUES ($insert_params)");

        foreach ($columns as $column)
            $statement->bindParam(':' . $column, $customer[$column]);

        $statement->execute();

        return $this->pdo->lastInsertId();
    }

    /**
     * @param array $credit_card
     * @param int $customer_id
     */
    protected function importCreditCard(array $credit_card, int $customer_id)
    {
        $card_expiration_date = explode("/", $credit_card['expirationDate']);

        $credit_card['expiration_month'] = $card_expiration_date[0];
        $credit_card['expiration_year'] = $card_expiration_date[1];

        $credit_card['customer_id'] = $customer_id;

        $columns = (new CreditCard())->getFillable();

        $insert_columns = '`' . implode('`,`', $columns) . '`';
        $insert_params = ':' . implode(',:', $columns);

        $statement = $this->pdo->prepare("INSERT INTO `credit_cards` ({$insert_columns}) VALUES ($insert_params)");

        foreach ($columns as $column)
            $statement->bindParam(':' . $column, $credit_card[$column]);

        $statement->execute();
    }

    /**
     * @param array $customers
     */
    public function import(array $customers)
    {
        $this->pdo->beginTransaction();
        foreach ($customers as $customer) {
            $customer_id = $this->importCustomer($customer);
            $this->importCreditCard($customer['credit_card'], $customer_id);
        }
        $this->pdo->commit();
    }
}
