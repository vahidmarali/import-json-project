<?php

namespace App\Services;

use App\Models\Customer;

class CustomerImportEloquent implements CustomerImportRepository
{
    /**
     * @param array $customers
     */
    public function import(array $customers)
    {
        foreach ($customers as $customer) {
            $customer_record = Customer::create([
                "name" => $customer["name"],
                "address" => $customer["address"],
                "checked" => $customer["checked"],
                "description" => $customer["description"],
                "interest" => $customer["interest"],
                "date_of_birth" => $customer["date_of_birth"],
                "email" => $customer["email"],
                "account" => $customer["account"],
            ]);
            $expiration_date = explode("/", $customer['credit_card']['expirationDate']);
            $customer_record->creditCard()->create([
                "type" => $customer['credit_card']["type"],
                "number" => $customer['credit_card']["number"],
                "name" => $customer['credit_card']["name"],
                "expiration_month" => $expiration_date[0],
                "expiration_year" => $expiration_date[1],
            ]);
        }
    }
}
