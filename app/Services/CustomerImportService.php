<?php

namespace App\Services;

use App\Jobs\HandleImportCustomers;
use Carbon\Carbon;
use http\Exception\InvalidArgumentException;

class CustomerImportService
{
    /**
     * @var CustomerImportRepository
     */
    protected CustomerImportRepository $customerImport;

    /**
     * @var JsonReaderInterface
     */
    protected JsonReaderInterface $jsonReader;

    public function __construct(CustomerImportRepository $customerImport, JsonReaderInterface $jsonReader)
    {
        $this->customerImport = $customerImport;
        $this->jsonReader = $jsonReader;
    }

    /**
     * @param array $customers
     * @return array
     */
    protected function filterCustomers(array $customers)
    {
        return collect($customers)->filter(function ($customer){
            $birth = FormatDate::format($customer['date_of_birth']);

            if($this->canPassAgeLimits($birth)===false)
                return false;

            if($this->canPassCardNumberPattern($customer['credit_card']['number'])===false)
                return false;

            return true;
        })->toArray();
    }

    /**
     * @param string|null $date_of_birth
     * @return bool
     */
    protected function canPassAgeLimits(string|null $date_of_birth) : bool
    {
        if(is_null($date_of_birth)){
            return config('customers.import_unknown_ages');
        }

        $age = (int) Carbon::parse($date_of_birth)->diff(Carbon::now())->format('%y');

        if(
            (!config('customers.min_age') || $age >= config('customers.min_age'))
            &&
            (!config('customers.max_age') || $age <= config('customers.max_age'))
        ){
            return true;
        }

        return false;
    }

    /**
     * @param string $card_number
     * @return bool
     */
    protected function canPassCardNumberPattern(string $card_number) : bool
    {
        if(!config('customers.limit_cart_regex_pattern') || preg_match(config('customers.limit_cart_regex_pattern'), $card_number)){
            return true;
        }
        return false;
    }

    /**
     * @param string $path
     * @param int $offset
     */
    public function handle(string $path, int $offset = 0)
    {
        $customers_from_json = $this->jsonReader->from($path)->read(config('customers.import_string_length'), $offset);
        $this->customerImport->import($this->filterCustomers($customers_from_json));
        if($this->jsonReader->nextOffset()!==0)
            HandleImportCustomers::dispatch($path, $this->jsonReader->nextOffset());
    }
}
