<?php

namespace App\Services;

interface JsonReaderInterface
{
    /**
     * @param string $path
     * @return self
     */
    public function from(string $path): self;

    /**
     * @param int $length
     * @param int $offset
     * @return array
     */
    public function read(int $length = 0, int $offset = 0): array;

    /**
     * @return int
     */
    public function nextOffset() : int;
}
