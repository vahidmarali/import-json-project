<?php

namespace App\Jobs;

use App\Services\CustomerImportService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class HandleImportCustomers implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected string $path;


    /**
     * @var int
     */
    protected int $offset;

    /**
     * Create a new job instance.
     * @param string $path
     * @param int $offset
     */
    public function __construct(string $path, int $offset = 0)
    {
        $this->path = $path;
        $this->offset = $offset;
    }

    /**
     * The number of seconds after which the job's unique lock will be released.
     * @var int
     */
    public int $uniqueFor = 3600;

    /**
     * The unique ID of the job.
     * @return string
     */
    public function uniqueId() : string
    {
        return $this->path.$this->offset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CustomerImportService $customerImport)
    {
        $customerImport->handle($this->path, $this->offset);
    }
}
