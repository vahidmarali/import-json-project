<?php

namespace App\Console\Commands;

use App\Jobs\HandleImportCustomers;
use App\Services\CustomerImportRepository;
use App\Services\JsonReaderInterface;
use Illuminate\Console\Command;
use InvalidArgumentException;

class JsonCustomerImporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:import {storage_path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Put a file to be imported into customers';

    /**
     * @return int
     */
    public function handle()
    {
        $file = storage_path($this->argument('storage_path'));
        if(!file_exists($file))
            throw new InvalidArgumentException("File \"{$file}\" not found!");
        HandleImportCustomers::dispatch($file);
    }
}
