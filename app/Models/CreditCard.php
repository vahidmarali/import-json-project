<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CreditCard Model
 * @property int $id
 * @property int $customer_id
 * @property string $type
 * @property string $number
 * @property string $name
 * @property int $expiration_month
 * @property int $expiration_year
 */
class CreditCard extends Model
{
    /**
     * @var array
     */
    protected $fillable = ["customer_id", "type", "number", "name", "expiration_month", "expiration_year"];

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }
}
