<?php

namespace App\Models;

use App\Services\FormatDate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Customer Model
 * @property int $id
 * @property string $name
 * @property string|null $address
 * @property bool $checked
 * @property string|null $description
 * @property string|null $interest
 * @property string|null $date_of_birth
 * @property string $email
 * @property string $account
 */
class Customer extends Model
{
    /**
     * @var array
     */
    protected $fillable = ["name","address","checked","description","interest","date_of_birth","email","account"];

    public $timestamps = false;

    /**
     * Mutate date to save with the right format
     * @param string|null $value
     */
    public function setDateOfBirthAttribute(string|null $value): void
    {
        $this->attributes['date_of_birth'] = FormatDate::format($value);
    }

    /**
     * @return HasOne
     */
    public function creditCard(): HasOne
    {
        return $this->hasOne(CreditCard::class);
    }
}
