<?php

namespace App\Providers;

use App\Services\CustomerImportPDO;
use App\Services\CustomerImportRepository;
use App\Services\JsonReaderInterface;
use App\Services\JsonReaderStream;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CustomerImportRepository::class, CustomerImportPDO::class);
        $this->app->bind(JsonReaderInterface::class, JsonReaderStream::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
