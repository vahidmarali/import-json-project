<?php

return [
    /**
     * Minimum age of the customer that should be imported
     * If it were set to null or 0 or false, it means that there is no minimum age limit
     */
    'min_age' => 18,

    /**
     * Maximum age of the customer that should be imported
     * If it were set to null or 0 or false, it means that there is no maximum age limit
     */
    'max_age' => 65,

    /**
     * Set if customers without information about their age should be imported
     */
    'import_unknown_ages' => true,

    /**
     * We can set a regex to check customer's card number
     * If it were set to null, it means that there is no pattern limit for customer's card_number
     * For example set it to '/(.)\\1{2}/' for three consecutive same digits
     */
    'limit_cart_regex_pattern' => '/(.)\\1{2}/',

    /**
     * The length of the string that should be parsed and imported on every request.
     * If it were set to null, it means that the entire records should be imported immediately.
     * Be careful of setting this, Because if server couldn't handle them on a request,
     * no customer would be imported.
     */
    'import_string_length' => 1000000
];
